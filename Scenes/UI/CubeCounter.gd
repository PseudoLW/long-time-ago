extends Label

func _ready():
	pass

func _process(_delta):
	if (!PlayerInventory.touched_cube):
		self.visible = false
	else:
		if (!self.visible):
			self.visible = true
		self.text = "cube in hand: " + str(PlayerInventory.total_cube)
