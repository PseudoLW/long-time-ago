extends StaticBody2D

var off = true

func _process(_delta):
	if off:
		$AnimatedSprite.play("off")
	else:
		$AnimatedSprite.play("on")
	
func _on_interact(_body):
	if off and PlayerInventory.total_cube>0:
		off=false
		PlayerInventory.total_cube_open_door+=1
		PlayerInventory.total_cube-=1
	elif !off:
		off=true
		PlayerInventory.total_cube_open_door-=1
		PlayerInventory.total_cube+=1
	pass
