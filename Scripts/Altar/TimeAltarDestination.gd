extends StaticBody2D

var source

func _ready():
	$AnimatedSprite.play("on")
	source = get_parent().get_global_position()
	
func _on_interact(body):
	PlayerInventory.total_cube += 1
	AudioPlayer.swap('present')
	body.interact_altar(fposmod(global_position.x, 1024), fposmod(global_position.y, 608))
	body.set_position(source)
	print(body.test_move(body.transform, Vector2(0, -1)))
	
