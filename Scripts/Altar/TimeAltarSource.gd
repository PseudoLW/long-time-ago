extends StaticBody2D

var destination

func _ready():
	$AnimatedSprite.play("off")
	destination = get_children()[2]
	
func _on_interact(body):
	if PlayerInventory.total_cube > 0:
		PlayerInventory.total_cube -= 1
		AudioPlayer.swap('past')
		body.interact_altar(fposmod(global_position.x, 1024), fposmod(global_position.y, 608))
		body.set_position(destination.get_global_position())
		print(body.test_move(body.transform, Vector2(0, -1)))
