extends KinematicBody2D

var velocity = Vector2(0, 0)
var collider = null

var transitioning = false

var timeCube = preload("res://Scenes/TimeCube.tscn")
var jumpTimer = 0

var finish = false
var speed = 600

class_name PlayerCharacter
signal interact_altar(x, y)
signal interact_door(side)

func drop_cube():
	var timeCube_instance = timeCube.instance()
	timeCube_instance.position = $TimeCubePoint.get_global_position()
	get_parent().call_deferred('add_child',timeCube_instance)
	
	
func _physics_process(delta):
	if finish:
		return
	# Input
	var pressedButton = false
	var skidding = false

	if Input.is_action_pressed("moveLeft"):
		velocity.x -= speed * delta
		if (velocity.x > 0):
			velocity.x -= (speed) * delta
			skidding = true
		pressedButton = true
		$RayCast2D.rotation_degrees = 90
		$TimeCubePoint.position.x = -32

	if Input.is_action_pressed("moveRight"):
		if (velocity.x < 0):
			velocity.x += speed * delta
			skidding = true
		velocity.x += speed * delta
		pressedButton = true
		$RayCast2D.rotation_degrees = - 90
		$TimeCubePoint.position.x = 32
	
	# Jump
	if Input.is_action_just_pressed("jump") and is_on_floor():
		jumpTimer = 200
		velocity.y = -550
		if (skidding):
			velocity.x = 0
			skidding = false
	
	if !transitioning:
		if Input.is_action_just_pressed("pickOrDrop") and $RayCast2D.is_colliding():
			collider = $RayCast2D.get_collider()
			collider._on_interact(self)
		elif Input.is_action_just_pressed("pickOrDrop") and !$RayCast2D.is_colliding():
			if PlayerInventory.total_cube>0 and is_on_floor():
				PlayerInventory.total_cube-=1
				drop_cube()
	
	if jumpTimer > 0:
		jumpTimer -= 1
	# Friction
	if not pressedButton:
		if(is_on_floor()):
			velocity.x -= 3.5 * delta * velocity.x
		else:
			velocity.x -= 2 * delta * velocity.x
		if abs(velocity.x) < 50:
			velocity.x = 0
	velocity.x = clamp(velocity.x, -350, 350)
	
	if (
		jumpTimer > 0
		and !Input.is_action_pressed("jump") 
		and velocity.y < 0
	):
		velocity.y += 3000 * delta
	else:
		velocity.y += 1300 * delta
	velocity = move_and_slide(velocity, Vector2(0, -1), true)

	$AnimationManager.set_sprite(velocity, skidding, not is_on_floor())


func interact_door(side):
	emit_signal("interact_door", side)
	transitioning = true

func interact_altar(x, y):
	emit_signal("interact_altar", x, y)
	transitioning = true

func finish_transition():
	transitioning = false
