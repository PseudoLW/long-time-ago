extends Area2D


export(String, "move", "jump", "interact") var type

var current_player

func _ready():
	current_player = get_parent().get_node("Chara")
	if overlaps_body(current_player):
		$AnimatedSprite.show()
	else:
		$AnimatedSprite.hide()
	
	$AnimatedSprite.play(type)


func _on_ControlHint_body_entered(body):
	if body is PlayerCharacter:
		$AnimatedSprite.show()


func _on_ControlHint_body_exited(body):
	if body is PlayerCharacter:
		$AnimatedSprite.hide()

var t = 0
func _process(_delta):
	$AnimatedSprite.position.y = -85 + 8 * sin(2 * PI * t / 360)
	t += 4
	if t > 360:
		t = 0
