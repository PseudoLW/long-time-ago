extends StaticBody2D

const screenWidth = 1024

var source
var side

func _ready():
	source = get_parent().get_global_position()
	if fposmod(global_position.x, screenWidth) < screenWidth / 2.0:
		side = 'left'
	else:
		side = 'right'
	
func _on_interact(body):
	body.interact_door(side)
	body.set_position(source)
	
