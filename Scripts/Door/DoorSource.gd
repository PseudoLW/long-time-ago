extends StaticBody2D

const screenWidth = 1024

var side
var destination

func _ready():
	$AnimatedSprite.play("Source")
	destination = get_children()[2]
	if fposmod(global_position.x, screenWidth) < screenWidth / 2.0:
		side = 'left'
	else:
		side = 'right'
	
func _on_interact(body):
	body.interact_door(side)
	body.set_position(destination.get_global_position())
