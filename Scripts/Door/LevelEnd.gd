extends StaticBody2D

export (String) var sceneName = "PrototypeLevel"
export (String) var spriteType = "future"
export (int) var totalcube=2
var Closed = true

signal win(destination)

func _process(_d):
	if Closed:
		if spriteType=="future":
			$AnimatedSprite.play("Closed1")
		else:
			$AnimatedSprite.play("Closed")
	else:
		if spriteType=="future":
			$AnimatedSprite.play("Open1")
		else:
			$AnimatedSprite.play("Open")
	if PlayerInventory.total_cube_open_door>=totalcube:
		Closed = false
	else:
		Closed = true

func _on_interact(_b):
	if !Closed:
		emit_signal("win", sceneName)
