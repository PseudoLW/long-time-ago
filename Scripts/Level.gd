extends Node2D

signal interact_altar(x, y)
signal interact_door(side)
signal win(next_scene)

var debug_mode = true
var is_level = true

func _on_Chara_interact_altar(x, y):
	emit_signal("interact_altar", x, y)
	if (debug_mode):
		yield(get_tree(), "idle_frame")
		finish_transition()

func _on_Chara_interact_door(side):
	emit_signal("interact_door", side)
	if (debug_mode):
		yield(get_tree(), "idle_frame")
		finish_transition()

func get_cam_pos():
	return $TileMap/Chara/Camera2D.global_position

func finish_transition():
	$TileMap/Chara.finish_transition()

func _on_FinalDoor_win(sceneName):
	emit_signal("win", sceneName)
	if debug_mode:
		PlayerInventory.total_cube_open_door=0
		var _status = get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
	$TileMap/Chara.visible = false
	$TileMap/Chara.finish = true
	
