extends Node

var switcher_volume = 1
var global_volume = 1
var changing = 0

func play():
	$Past.play()
	$Present.play()
	AudioServer.set_bus_volume_db(2, -80)
	AudioServer.set_bus_volume_db(1, 0)
	
var countdown = 0

func stop():
	$Past.stop()
	$Present.stop()

func _process(_delta):
	if changing != 0:
		switcher_volume += changing * 0.01
		if switcher_volume > 1:
			switcher_volume = 1
			changing = 0
		elif switcher_volume < 0:
			switcher_volume = 0
			changing = 0
		
		var t1 = sqrt(2 * switcher_volume - switcher_volume*switcher_volume)
		var t2 = sqrt(1 - switcher_volume*switcher_volume)
		t1 *= global_volume
		t2 *= global_volume
		AudioServer.set_bus_volume_db(1, lerp(-80, 0, t1))
		AudioServer.set_bus_volume_db(2, lerp(-80, 0, t2))

func swap(newMode):
	if newMode == 'past':
		changing = -1
	elif newMode == 'present':
		changing = 1
