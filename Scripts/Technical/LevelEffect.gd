extends Node

var world = null

const width = 1024
const height = 608

signal ctrl_pressed

func _ready():
	change_level('Level0')
	$EffectViewport.world_2d = $WorldViewport.world_2d

func _process(_delta):
	if Input.is_action_just_pressed("pickOrDrop"):
		emit_signal("ctrl_pressed")

func change_level(level_name):
	PlayerInventory.total_cube_open_door = 0
	$Main.material.set_shader_param("r",  1)
	if world != null:
		$WorldViewport.remove_child(world)
		world.queue_free()
	world = load('res://Scenes/Level/' + level_name + '.tscn').instance()
	$WorldViewport.add_child(world)
	world.debug_mode = false
	world.connect('interact_altar', self, 'interact_altar')
	world.connect('interact_door', self, 'interact_door')
	world.connect('win', self, 'win')
	$EffectViewport/Camera2D.global_position = world.get_cam_pos()
	$Effect.visible =false
	var t = 1
	$Main.material.set_shader_param('l', 1)
	while t > 0:
		var true_t = (1 - cos(t*PI))/2.0
		$Main.material.set_shader_param('l', true_t)
		t -= 0.05
		yield(get_tree(),"idle_frame")
	$Main.material.set_shader_param('l', 0)


func interact_altar(x, y):
	$Effect.visible = true
	$Effect.material.set_shader_param("c", Vector2(x, height - y))
	var t = 0
	while t < 1:
		var true_t = t
		var r1 = lerp(0, 2048, true_t)
		$Effect.material.set_shader_param("r1", r1)
		$Effect.material.set_shader_param("r2", lerp(0, 1024, true_t))
		t += 0.04
		yield(get_tree(),"idle_frame")

	$Effect.visible = false
	$EffectViewport/Camera2D.global_position = world.get_cam_pos()
	$Effect.material.set_shader_param("r1", 0)
	$Effect.material.set_shader_param("r2", 0)
	world.finish_transition()

func interact_door(slide):
	$Effect.visible = true
	var slide_direction
	var t = 1
	if (slide == 'left'):
		slide_direction = 1
	elif (slide == 'right'):
		slide_direction = -1
	var main2_src = 0
	var main2_dest = 0 + slide_direction
	var main_src = -slide_direction
	var main_dest = 0
		
	while t > 0:
		var true_t = (1 - cos((1 - t)*PI))/2.0
		#warning-ignore:integer_division
		$Main.position.x = lerp(
			main_src * width,
			main_dest * width,
			true_t
		) + width / 2
		#warning-ignore:integer_division
		$Effect.position.x = lerp(
			main2_src * width,
			main2_dest * width,
			true_t
		) + width / 2
		t -= 0.07
		yield(get_tree(),"idle_frame")
	#warning-ignore:integer_division
	$Main.position.x = width / 2
	#warning-ignore:integer_division
	$Effect.position.x = width / 2
	$Effect.visible = false
	$EffectViewport/Camera2D.global_position = world.get_cam_pos()
	world.finish_transition()

func win(level_name):
	$LevelComplete.visible = true
	var t = 0
	while t < 1:
		$Main.material.set_shader_param(
			"brightness", 
			lerp(1, 0.4, t)
		)
		yield(get_tree(),"idle_frame")
		t += 0.09
	yield(self, "ctrl_pressed")

	$LevelComplete.visible = false
	
	if level_name.substr(0, 5) != 'Level':
		var _d = get_tree().change_scene("res://Scenes/MainMenu.tscn")
		return
	level_name.erase(0, 6)
	
	while t < 1:
		$Main.material.set_shader_param("r",  lerp(1, 0, t))
		t += 0.08
		yield(get_tree(),"idle_frame")
	$Main.material.set_shader_param("r",  lerp(1, 0, t))
	$Main.material.set_shader_param("brightness", 1)
	
	change_level(level_name)
