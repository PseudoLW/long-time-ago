shader_type canvas_item;

uniform float l = 0;
uniform float r = 1;
uniform float brightness = 1;

void fragment(){
	COLOR = texture(TEXTURE, UV);
	COLOR.rgb *= brightness;
	COLOR.rgb *= float(UV.x > l);
	COLOR.rgb *= float(UV.x < r);
}