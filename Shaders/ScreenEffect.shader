shader_type canvas_item;

uniform vec2 c = vec2(0, 0); // Y inverted!!!
uniform float r1 = 0; // Outer circle
uniform float r2 = 0; // Inner circle

const vec2 screenSize = vec2(1024.0, 608.0);

void fragment(){
	COLOR = texture(TEXTURE, UV);

	vec2 d1_uv = r1 / screenSize;
	vec2 d2_uv = r2 / screenSize;
	vec2 c_uv = c / screenSize;

	vec2 coord_square = (UV - c_uv) * (UV - c_uv);
	float ellipse_factor = dot(coord_square, screenSize * screenSize);
	float dist_1 = ellipse_factor / (r1 * r1);
	float dist_2 = ellipse_factor / (r2 * r2);

	COLOR.a *= float(dist_2 > 1.0);
	COLOR.rgb = 
		COLOR.rgb * float(dist_1 > 1.0)
		+ (vec3(1) - COLOR.rgb) * (vec3(1) - float(dist_1 > 1.0));

}